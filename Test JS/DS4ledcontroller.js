#!/usr/bin/gjs

//import lib
imports.gi.versions.Gtk = '3.0';
imports.gi.versions.WebKit2 = '4.0';
//lib colors
imports.gi.versions.Gdk = '3.0';

//lib
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;
const Webkit = imports.gi.WebKit2;

// scale (slider)
const Gio = imports.gi.Gio;
//const Gtk = imports.gi.Gtk;

// color
const Gdk = imports.gi.Gdk;
const GObject = imports.gi.GObject;
//const Gtk = imports.gi.Gtk;

// Uint8Array
const ByteArray = imports.byteArray;

const Struct = (...keys) => ((...v) => keys.reduce((o, k, i) => {o[k] = v[i]; return o} , {}))
const _Device = Struct ('deviceDir', 'deviceUname', 'deviceRenamed','red','green','blue')

//app OBJECT ORIENTED : the entire program is a class called DS4LedController
class DS4LedController {

	// Create the application itself
	constructor() {
		this.application = new Gtk.Application();

		// Connect 'activate' and 'startup' signals to the callback functions
		this.application.connect('activate', this._onActivate.bind(this));
		this.application.connect('startup', this._onStartup.bind(this));
	}

	// Callback function for 'activate' signal presents windows when active
	_onActivate() {
		this._window.present();
	}

	// Callback function for 'startup' signal builds the UI
	_onStartup() {
		this._buildUI ();
	}

	// Build the application's UI
	_buildUI() {

		// Create the application window
		this._window = new Gtk.ApplicationWindow({
			application: this.application,
			window_position: Gtk.WindowPosition.CENTER,
			border_width: 10,
			default_height: 200,
			default_width: 400,
			title: "DS4 Led controller 4 GNOME"});

		// Create the Grid
		this._grid = new Gtk.Grid ({
			//column_homogeneous: true,
			// column_spacing: 20,
			row_homogeneous: true,
			row_spacing: 20 });

		// Create an image
		//this._image = new Gtk.Image ({ file: "gnome-image.png" });

		// Create a second image using a stock icon
		//this._icon = new Gtk.Image ({ stock: 'gtk-about' });

		// Create a menu with a list of devices
		this._ds4List = new Gtk.ComboBoxText();

		// Connect the combobox's 'changed' signal to our callback function
		this._ds4List.connect ('changed', this._onDS4Selected.bind(this));

		//reload list connected DS4 button
		this._reloadDS4list = new Gtk.Button({label: "Reload"})

		// Connect the reload list
		this._reloadDS4list.connect ('clicked', this._findDS4.bind(this));

		//rename current list item
		this._renameButton = new Gtk.Button({label: "Rename"})

		// Connect the reload list
		this._renameButton.connect ('clicked', this._renameDevice.bind(this));

		// Create a label
		this._colorlabel = new Gtk.Label ({
			label: "Color",
			/*
			halign: Gtk.Align.CENTER,
			valign: Gtk.Align.CENTER,
			margin_top: 20,
			margin_left: 20 */ });

		// Create the horizontal scale 4 color
		this._adjustmentA = new Gtk.Adjustment ({
			value: 0.5,
			lower: 0,
			upper: 1,
			step_increment: 0.1,
			page_increment: 0.2 });

		this._cScale = new Gtk.Scale ({
			orientation: Gtk.Orientation.HORIZONTAL,
			adjustment: this._adjustmentA,
			digits: 1,
			draw_value: false
			//margin_left: 10
			});

		this._cScale.set_value (1.0);
		// this element change and adapt the grid to window size
		this._cScale.set_hexpand (true);
		
		// Create a second label
		this._saturationlabel = new Gtk.Label ({
			label: "Saturation" });

		// Create the horizontal scale 4 brightness
		// Create a master adjustment to use for the brightness (or any other) scale
		this._adjustmentB = new Gtk.Adjustment ({
			value: 0.5,
			lower: 0,
			upper: 1,
			step_increment: 0.1,
			page_increment: 0.2 });

		this._sScale = new Gtk.Scale ({
			orientation: Gtk.Orientation.HORIZONTAL,
			adjustment: this._adjustmentB,
			digits: 1,
			draw_value: false
			//margin_left: 10
			});
		this._sScale.set_value (1.0);

		// Create a third label
		this._brightnesslabel = new Gtk.Label ({
			label: "Brightness" });
		this._adjustmentC = new Gtk.Adjustment ({
			value: 0.5,
			lower: 0,
			upper: 1,
			step_increment: 0.1,
			page_increment: 0.2 });

		this._bScale = new Gtk.Scale ({
			orientation: Gtk.Orientation.HORIZONTAL,
			adjustment: this._adjustmentC,
			digits: 1,
			draw_value: false
			//margin_left: 10
			});

		this._bScale.set_value (1);

		// Connect the three scales to functions which recalculate the label
		this._cScale.connect ("value-changed", this._recalc.bind(this));
		this._sScale.connect ("value-changed", this._recalc.bind(this));
		this._bScale.connect ("value-changed", this._recalc.bind(this));
		// Connect the three scales to a finction that dave the final state
		this._cScale.connect ("button-release-event", this._saveStatus.bind(this));
		this._sScale.connect ("button-release-event", this._saveStatus.bind(this));
		this._bScale.connect ("button-release-event", this._saveStatus.bind(this));

		// Create a color selector
		this._colorbutton = new Gtk.ColorButton();
		//this._colorbutton.set_sensitive(false);
		var color = new Gdk.RGBA();
		color.red = 0.0;
		color.green = 0.0;
		color.blue = 0.7;
		color.alpha = 1;
		this._colorbutton.set_rgba(color);
		this._colorbutton.connect("color-set", this._onColorChosen.bind(this));

		// Attach the images and button to the grid
		// col , row , dc, dr
		this._grid.attach (this._reloadDS4list,  0, 0, 1, 1);
		this._grid.attach (this._ds4List,  1, 0, 1, 1);
		this._grid.attach (this._renameButton,  2, 0, 1, 1);
		this._grid.attach (this._colorbutton, 0, 1, 3, 1);
		this._grid.attach (this._colorlabel,  0, 2, 1, 1);
		this._grid.attach (this._saturationlabel,  0, 3, 1, 1);
		this._grid.attach (this._brightnesslabel,  0, 4, 1, 1);
		this._grid.attach (this._cScale, 1, 2, 2, 1);
		this._grid.attach (this._sScale, 1, 3, 2, 1);
		this._grid.attach (this._bScale, 1, 4, 2, 1);
		
		// Add the grid to the window
		this._window.add (this._grid);

		// Show the window and all child widgets
		this._window.show_all();
		this._loadStatus()

		// current device set at 'Select DS4'
		// not like this...
		//this._currentDevice = this._database[0]
		// like this
		this._currentDevice=_Device(null,'Select DS4','new name',0,0,0.7)
		this._findDS4()
	}

	_recalc() {
		//recalcolate color rgb from hsv values
		var h = this._cScale.get_value()
		var s = this._sScale.get_value()
		var v = this._bScale.get_value()
		var r, g, b, i, f, p, q, t;
		i = Math.floor(h * 6);
		f = h * 6 - i;
		p = v * (1 - s);
		q = v * (1 - f * s);
		t = v * (1 - (1 - f) * s);
		switch (i % 6) {
			case 0: r = v, g = t, b = p; break;
			case 1: r = q, g = v, b = p; break;
			case 2: r = p, g = v, b = t; break;
			case 3: r = p, g = q, b = v; break;
			case 4: r = t, g = p, b = v; break;
			case 5: r = v, g = p, b = q; break;
		}
		var color = new Gdk.RGBA();
		color.red = r
		color.green = g
		color.blue = b
		color.alpha = 1
		this._colorbutton.set_rgba(color)
		this._currentDevice.red = r
		this._currentDevice.green = g
		this._currentDevice.blue = b	
		this._sendToDevice()
	}

	_onColorChosen(){
		var color = this._colorbutton.get_rgba()
		this._currentDevice.red = color.red
		this._currentDevice.green = color.green
		this._currentDevice.blue = color.blue	
		this._UIelementsToCurrentColor()
		this._sendToDevice()
		this._saveStatus()
	}

	_renameDevice(){
		// Which combobox item is active?
		let activeItem = this._ds4List.get_active();
		if (activeItem == -1)return
		let devicetoberenamedUname=this._listDevices[activeItem].deviceUname
		if(devicetoberenamedUname=="Select DS4" || devicetoberenamedUname=="generic controller"){
			print ("cant rename");
			return
		}
		//change current device to active item
		this._entryPopup()
	}

	_entryPopup() {
        // Create a popup dialog to greet the person who types in their name
        this._popup = new Gtk.ApplicationWindow({
            application: this.application,
            window_position: Gtk.WindowPosition.CENTER,
            title: 'Please enter your device name:',
            default_height: 100,
            default_width: 300,
            border_width: 20,
        });
		// Create the text entry box
		var vbox = new Gtk.VBox()
        this._entry = new Gtk.Entry ();
        this._entry.set_text = "original name string"
		this._popup.add(this._entry);
        // Show the popup dialog
        //dialog.vbox.pack_end(hbox
        this._popup.show_all();
        // Bind the OK button to the function that closes the popup
        this._entry.connect ("activate", this._okClicked.bind(this));
    }

	_okClicked() {
		var newname= this._entry.get_text()
		let activeItem = this._ds4List.get_active();
		// verify exist a save state for the device selected
		for (let i = 0; i < this._database.length; i++){
			if(this._database[i].deviceUname==this._listDevices[activeItem].deviceUname) {
				this._database[i].deviceRenamed=newname
				//this._currentDevice.deviceRenamed = newname
				this._saveStatus()
				this._refreshList()
				this._popup.destroy();
				return
			}
		}
		this._popup.destroy();
	}

	_findDS4(){
		//  Need to find device dir, id unique name etc
		this._listDevices = []
		this._listDevices = [
			// device 0 null, is the Select DS4
			_Device(null,'Select DS4',null,null,null,null)
			//_Device(null,'1','new name')
		]
		let [res, stringJsConnected] = GLib.spawn_command_line_sync('/bin/bash -c "ls /dev/input/ | grep js"')
		// grep ... output is // Uint8Array
		if (stringJsConnected.length==0) {
			// no devices found
			this._refreshList();
			return
		}
		var arrayJsConnected = ByteArray.toString(stringJsConnected)
		// build the list
		let listJs = []
		let nameJs=""
		for (let i = 0; i < arrayJsConnected.length; i++){
			if (arrayJsConnected[i]=="\n"){
				listJs.push(nameJs)
				nameJs=""
			} else {
				nameJs+=arrayJsConnected[i]
			}
		}
		// for every device try see if is a DS4
		for(let i=0; i < listJs.length; i++){
			//let string1 ='udevadm info -a -p $(udevadm info -q path -n /dev/input/js0) | egrep -o KERNELS==[[:punct:]][[:xdigit:]]{4}:054C:09CC\.[[:xdigit:]]{4}'
			//let string2 ='udevadm info -a -p $(udevadm info -q path -n /dev/input/js0) | egrep -o KERNELS==[[:punct:]][[:xdigit:]]{4}:054C:05C4\.[[:xdigit:]]{4}'
			let string1 ='/bin/bash -c "udevadm info -a -p $(udevadm info -q path -n /dev/input/'+listJs[i]+') | egrep -o KERNELS==[[:punct:]][[:xdigit:]]{4}:054C:09CC\.[[:xdigit:]]{4}"'
			let string2 ='/bin/bash -c "udevadm info -a -p $(udevadm info -q path -n /dev/input/'+listJs[i]+') | egrep -o KERNELS==[[:punct:]][[:xdigit:]]{4}:054C:05C4\.[[:xdigit:]]{4}"'
			// find kernel attrib (contain device DIR name)
			let [, stringKern09CC] = GLib.spawn_command_line_sync(string1)
			let [, stringKern05C4] = GLib.spawn_command_line_sync(string2)
			let string3 = '/bin/bash -c "udevadm info -a -p $(udevadm info -q path -n /dev/input/'+listJs[i]+') |  grep uniq"'
			let [, stringUname05C4] = GLib.spawn_command_line_sync(string3)
			var stringUname = ByteArray.toString(stringUname05C4)
			var stringDir
			if(stringKern05C4.length!=0){
				// associate unique name id at directory name
				stringDir = ByteArray.toString(stringKern05C4)
				stringDir=stringDir.substring(10,[29])
				stringUname=stringUname.substring(18,[35])
			} else {
				if(stringKern09CC.length!=0){
					// associate unique name id at directory name
					stringDir = ByteArray.toString(stringKern09CC)
					stringDir=stringDir.substring(10,[29])
					stringUname=stringUname.substring(18,[35])
				} else {
					//device not found
					stringDir=null
					stringUname="generic controller"
				}
			}
			this._listDevices.push(_Device(stringDir,stringUname,null,null,null,null))
		}
		this._refreshList();
	}

	_refreshList(){
		this._ds4List.remove_all ();
		for (let i = 0; i < this._listDevices.length; i++){
			var text=this._listDevices[i].deviceUname
			for (let j = 0; j < this._database.length; j++){
				if(this._database[j].deviceUname==text){
					if (this._database[j].deviceRenamed!='new name'){
						text=this._database[j].deviceRenamed
					}
				}
			}
			this._ds4List.append_text (text);
		}
		this._ds4List.set_active (0);
	}

	_onDS4Selected(){
		// Which combobox item is active?
		let activeItem = this._ds4List.get_active();
		if (activeItem == -1)return
		//change current device to active item
		this._currentDevice.deviceDir = this._listDevices[activeItem].deviceDir
    	this._currentDevice.deviceUname = this._listDevices[activeItem].deviceUname
		// verify exist a save state for the device selected
		for (let i = 0; i < this._database.length; i++){
			if(this._database[i].deviceUname==this._listDevices[activeItem].deviceUname) {
				// record device state
				this._currentDevice.red = this._database[i].red
				this._currentDevice.green = this._database[i].green
				this._currentDevice.blue = this._database[i].blue
       			this._sendToDevice()
				this._UIelementsToCurrentColor()
				return
			}
		}
		// a new item not found in database
		this._currentDevice.red = this._database[0].red
		this._currentDevice.green = this._database[0].green
		this._currentDevice.blue = this._database[0].blue
		//add device to database WITH slice in order to not copy a pointer
		this._database.push(_Device(null,this._currentDevice.deviceUname,'new name',this._currentDevice.red,this._currentDevice.green,this._currentDevice.blue))
		var i = this._database.length
		// add to database
		this._sendToDevice()
		this._UIelementsToCurrentColor()
	}
	
	// actualize button color and sliders to actual value 
	_UIelementsToCurrentColor(){
		//set colorbutton
		var color = new Gdk.RGBA();
		color.red = this._currentDevice.red
		color.green = this._currentDevice.green
		color.blue = this._currentDevice.blue
		color.alpha = 1
		this._colorbutton.set_rgba(color)
		// set sliders 
		let v=Math.max(color.red,color.green,color.blue), n=v-Math.min(color.red,color.green,color.blue);
		let h= n && ((v==color.red) ? (color.green-color.blue)/n : ((v==color.green) ? 2+(color.blue-color.red)/n : 4+(color.red-color.green)/n)); 
		let aaa = [60*(h<0?h+6:h), v&&n/v, v];
		h = aaa[0]/360
		let s = aaa[1]
		v = aaa[2]
		this._cScale.set_value(h)
		this._sScale.set_value(s)
		this._bScale.set_value(v)
	}


	_sendToDevice() {
		let g = Math.round(this._currentDevice.green*255);
		let r = Math.round(this._currentDevice.red*255);
		let b = Math.round(this._currentDevice.blue*255);
		if (this._currentDevice.deviceDir==null) {
			// Device not connected
			return 0;
		}
		//position program
		let directoryPath="/sys/class/leds/"+this._currentDevice.deviceDir+":green/";
		let directory = Gio.File.new_for_path(directoryPath);
		if (!directory.query_exists(null)) {
			print('Error: directory '+this._currentDevice.deviceDir+':green not found');
		}else{
			let filePath = GLib.build_filenamev([directoryPath, "brightness"]);
			let value = ''+g
			let args = ["sh", "-c","echo "+ "\"" +value +"\"" + " > "+"\"" +filePath+"\"" +" "];
			GLib.spawn_sync(null, args, null, GLib.SpawnFlags.SEARCH_PATH, null);
		}
		directoryPath="/sys/class/leds/"+this._currentDevice.deviceDir+":red/";
		directory = Gio.File.new_for_path(directoryPath);
		if (!directory.query_exists(null)) {
			print('Error: directory '+this._currentDevice.deviceDir+':red not found');
		}else{
			let filePath = GLib.build_filenamev([directoryPath, "brightness"]);
			let value = ''+r
			let args = ["sh", "-c","echo "+ "\"" +value +"\"" + " > "+"\"" +filePath+"\"" +" "];
			GLib.spawn_sync(null, args, null, GLib.SpawnFlags.SEARCH_PATH, null);
		}
		directoryPath="/sys/class/leds/"+this._currentDevice.deviceDir+":blue/";
		directory = Gio.File.new_for_path(directoryPath);
		if (!directory.query_exists(null)) {
			print('Error: directory '+this._currentDevice.deviceDir+':blue not found');
		}else{
			let filePath = GLib.build_filenamev([directoryPath, "brightness"]);
			let value = ''+b
			let args = ["sh", "-c","echo "+ "\"" +value +"\"" + " > "+"\"" +filePath+"\"" +" "];
			GLib.spawn_sync(null, args, null, GLib.SpawnFlags.SEARCH_PATH, null);
		}
	}

	_saveStatus() {
		//save database
		for (let i = 0; i < this._database.length; i++){
			if(this._database[i].deviceUname==this._currentDevice.deviceUname) {
				// record device state
				this._database[i].red	= this._currentDevice.red
				this._database[i].green	= this._currentDevice.green
				this._database[i].blue	= this._currentDevice.blue
			}
		}
		let dataJSON = JSON.stringify(this._database);
		// Using .config in home
		let dataDir = GLib.get_user_config_dir();
		// current folder
		//dataDir = GLib.get_current_dir();
		let destination = GLib.build_filenamev([dataDir, 'DS4-Led', 'saved_conf.json'])
		let destinationFile = Gio.File.new_for_path(destination);
		var parent = destinationFile.get_parent();
		if (!parent.query_exists(null)){
			// folder doesn't exist
			// Creating conf saving directory
			parent.make_directory(null);
		}
		let [success, tag] = destinationFile.replace_contents(dataJSON, null, false, Gio.FileCreateFlags.REPLACE_DESTINATION, null);
		if(!success) {
			/* it failed */
			print("File "+destination+"not saved")
		}
	}

	_loadStatus(){
		this._database = []
		//read file and push elements into the database of devices
		// using .config in home
		let dataDir = GLib.get_user_config_dir();
		// current folder
		// dataDir = GLib.get_current_dir();
		let destination = GLib.build_filenamev([dataDir, 'DS4-Led', 'saved_conf.json'])
		let destinationFile = Gio.File.new_for_path(destination);
		if (!destinationFile.query_exists(null)) {
			//Creating new database
			this._database.push(_Device(null,'Select DS4','new name',0,0,0.7))
		}else{
			var data = ByteArray.toString(GLib.file_get_contents(destination)[1])
			this._database = JSON.parse(data)
		}
	}
};

// Run the application
let app = new DS4LedController ();
app.application.run (ARGV);
// start with: gjs DS4ledcontroller.js
