### License
This program is free software;  you can redistribute it and/or modify
it under the terms of the  GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or (at
your option) any later version.

This program is  distributed in the hope that it  will be useful, but
WITHOUT  ANY   WARRANTY;  without   even  the  implied   warranty  of
MERCHANTABILITY  or FITNESS FOR  A PARTICULAR  PURPOSE.  See  the GNU
General Public License for more details.

You should  have received  a copy of  the GNU General  Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.


# Install the shell extension ds4 LED controller

In order for the extension to work properly download and intall FIRST the udev rule (10-ds4led.rules) and freeDS4leds (made executable) bash script:

    https://gitlab.com/Fanelia82/ds4-led-controller/

ATTENTION!!!!
There is a change to the permissions of the led controlling files that can generate a security problem. Use wisely and on your own risk!
 
    sudo cp  /YOURDIRHERE/10-ds4led.rules /etc/udev/rules.d/
    sudo cp  /YOURDIRHERE/freeDS4leds /usr/local/bin/
    
Restart or

    sudo udevadm control --reload-rules
    
For your information here are the rules you just installed:
    
    # DualShock 4 over bluetooth hidraw
    KERNEL=="hidraw*", KERNELS=="*054C:05C4*", SUBSYSTEMS=="hid", MODE="0660", RUN+="/usr/local/bin/freeDS4leds '%p'"

    # DualShock 4 Slim over bluetooth hidraw
    KERNEL=="hidraw*", KERNELS=="*054C:09CC*", SUBSYSTEMS=="hid", MODE="0660", RUN+="/usr/local/bin/freeDS4leds '%p'"

Install the extension from extensions.gnome.org or copy the folder ds4-led-controller@fanelia82.org (gitlab repo) in:

	~/.local/share/gnome-shell/extensions/
	

# Test and debug
	
Need to test with different controllers
