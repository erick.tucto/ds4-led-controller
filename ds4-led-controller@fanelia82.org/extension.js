/**
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

	// Made by Me := Fanelia
	// This extension put a menu item on the Status Menu that change
	// the DS4 LED color and brightness
	// restart the shell in X11
	// Alt+F2 -> r

	// Extension log
	// Alt+F2 -> lg
	
	// Import St because is the library that allow you to create UI elements 
	const St = imports.gi.St;
	// Import Clutter because is the library that allow you to layout UI elements
	const Clutter = imports.gi.Clutter;
	// Import Main because is the instance of the class that have all the UI elements
	// and we have to add to the Main instance our UI elements
	const Main = imports.ui.main;
	// Import tweener to do the animations of the UI elements 
	//const Tweener = imports.ui.tweener;
	//Import PanelMenu and PopupMenu 
	const PanelMenu = imports.ui.panelMenu;
	const PopupMenu = imports.ui.popupMenu;
	const ShellEntry = imports.ui.shellEntry;
	const Gio = imports.gi.Gio;
	const Gtk = imports.gi.Gtk;
	const GLib = imports.gi.GLib;
	// color
	const Gdk = imports.gi.Gdk;
	// Uint8Array
	const ByteArray = imports.byteArray;
	const GObject = imports.gi.GObject;
	const ExtensionUtils = imports.misc.extensionUtils;
	const Me = ExtensionUtils.getCurrentExtension();
	// For compatibility checks, as described above
	const Config = imports.misc.config;
	const SHELL_MINOR = parseInt(Config.PACKAGE_VERSION.split('.')[1]);
	const Slider = imports.ui.slider;
	//Import Lang because we will write code in a Object Oriented Manner
	const Lang = imports.lang;

	const Struct = (...keys) => ((...v) => keys.reduce((o, k, i) => {o[k] = v[i]; return o} , {}))
	const _Device = Struct ('deviceDir', 'deviceUname', 'deviceRenamed','red','green','blue')

	// GLobal vairables
	const KEY_RETURN = 65293;
	const KEY_ENTER  = 65421;
	//let directory;
	
	// We're going to declare `indicator` in the scope of the whole script so it can
	// be accessed in both `enable()` and `disable()`
	//var indicator = null;
	// We'll extend the Button class from Panel Menu so we can do some setup in
	// the init() function.
	var ExampleIndicator = class ExampleIndicator extends PanelMenu.Button {
		_init() {
			super._init(0.0, `${Me.metadata.name} Indicator`, false)

			// Pick an icon
			let icon = new St.Icon({gicon: new Gio.ThemedIcon({name: 'applications-games-symbolic'}),style_class: 'system-status-icon'})

			this.actor.add_child(icon)
			this._indicator = null
			// Created menu icon

			// Menu expander
			this._popupMenuExpander = new PopupMenu.PopupSubMenuMenuItem('Select DS4');

			// Assemble all menu items
			this.menu.addMenuItem(this._popupMenuExpander);
			//menu closed
			this._popupMenuExpander.setSubmenuShown(true);
			// Relations main menu and submenus
			this._popupMenuExpander.connect('button-press-event', this._findDS4.bind(this));

			//remamer input text
			this._entry= new St.Entry({name: "newName",
					text:"Device name",
					track_hover: true,
					can_focus: true,
			})
			this._entry.clutter_text.set_max_length(18)
			this._entrymenuitem = new PopupMenu.PopupMenuSection()
			this._entrymenuitem.actor.add_actor(this._entry)
			this._entrymenuitem.actor.style_class = 'EntryStyle';
			this.menu.addMenuItem(this._entrymenuitem)

			// This is a menu separator
			this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

			// show current color in a box XXX pending
			//this._colorbuttonmenuitem = new PopupMenu.PopupMenuItem('');
			//this._testmenuitem.actor.add_actor(this._test)
			//this._colorbuttonmenuitem.actor.style_class = 'ButtonColorStyle';
			//this.menu.addMenuItem(this._colorbuttonmenuitem)

			let label1 = new St.Label({text:'H ',y_expand: true,y_align: Clutter.ActorAlign.START });
			let label2 = new St.Label({text:'S ',y_expand: true,y_align: Clutter.ActorAlign.START });
			let label3 = new St.Label({text:'V ',y_expand: true,y_align: Clutter.ActorAlign.START });
			
			// Slider color
			let colorH = new PopupMenu.PopupMenuItem('');
			this._cSlider = new Slider.Slider(1);
			//slider: new PopupMenu.PopupSliderMenuItem(0)
			
			colorH.actor.add(label1);
			colorH.actor.add(this._cSlider.actor, {expand: true});
			colorH.actor.style_class = 'ItemStyle';
			this.menu.addMenuItem(colorH);

			// Slider saturation
			let saturation = new PopupMenu.PopupMenuItem('');
			this._sSlider = new Slider.Slider(1);
			saturation.actor.add(label2);
			saturation.actor.add(this._sSlider.actor, {expand: true});
			saturation.actor.style_class = 'ItemStyle';
			this.menu.addMenuItem(saturation);

			// Slider brightness
			let brightness = new PopupMenu.PopupMenuItem('');
			this._bSlider = new Slider.Slider(1);
			
			brightness.actor.add(label3);
			brightness.actor.add(this._bSlider, {expand: true});
			brightness.actor.style_class = 'ItemStyle';
			this.menu.addMenuItem(brightness);

			// This is a menu separator
			this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
		
			// Sliders
			//sliderColor.connect('value-changed', this._onSliderChanged);
			//gnome <3.34
			this._cSlider.connect('notify::value', this._recalc.bind(this));
			this._sSlider.connect('notify::value', this._recalc.bind(this));
			this._bSlider.connect('notify::value', this._recalc.bind(this));
			// Connect the three scales to a finction that dave the final state
			this._cSlider.connect ("button-release-event", this._saveStatus.bind(this));
			this._sSlider.connect ("button-release-event", this._saveStatus.bind(this));
			this._bSlider.connect ("button-release-event", this._saveStatus.bind(this));
			//button rename function
			this._entry.clutter_text.connect('key-press-event', Lang.bind(this,function(o,e)
			{
				let symbol = e.get_key_symbol();
				if (symbol == KEY_RETURN || symbol == KEY_ENTER)
				{
					//Main.notify('Example Notification', 'Here i am')
					this._renameDevice()
				}
			}));

			this._loadStatus()
			this._currentDevice=_Device(null,'Select DS4','new name',0,0,0.7)
			this._findDS4()
		}	

		_recalc() {
			//recalcolate color rgb from hsv values
			//Main.notify('Example Notification', 'Here i am')
			var h = this._cSlider.value
			var s = this._sSlider.value
			var v = this._bSlider.value
			var r, g, b, i, f, p, q, t;
			i = Math.floor(h * 6);
			f = h * 6 - i;
			p = v * (1 - s);
			q = v * (1 - f * s);
			t = v * (1 - (1 - f) * s);
			switch (i % 6) {
				case 0: r = v, g = t, b = p; break;
				case 1: r = q, g = v, b = p; break;
				case 2: r = p, g = v, b = t; break;
				case 3: r = p, g = q, b = v; break;
				case 4: r = t, g = p, b = v; break;
				case 5: r = v, g = p, b = q; break;
			}

			this._currentDevice.red = r
			this._currentDevice.green = g
			this._currentDevice.blue = b
			this._sendToDevice()
		}

		_renameDevice(){
			if (this._activeItem == 0)return
			let devicetoberenamedUname=this._listDevices[this._activeItem].deviceUname
			if(devicetoberenamedUname=="Select DS4" || devicetoberenamedUname=="generic controller"){
				//print ("cant rename");
				return
			}
			//change current device to active item
			var newname= this._entry.get_text()
			//Main.notify('Example Notification', 'Here i am'+newname)
			// verify exist a save state for the device selected
			for (let i = 0; i < this._database.length; i++){
				if(this._database[i].deviceUname==this._listDevices[this._activeItem].deviceUname) {
					this._database[i].deviceRenamed=newname
					//this._currentDevice.deviceRenamed = newname
					this._saveStatus()
					this._refreshList()
					return
				}
			}
		}

		_findDS4(){
			//  Need to find device dir, id unique name etc
			this._listDevices = []
			this._listDevices = [
				// device 0 null, is the Select DS4
				_Device(null,'Select DS4',null,null,null,null)
				//_Device(null,'1','new name')
			]
			let [res, stringJsConnected] = GLib.spawn_command_line_sync('/bin/bash -c "ls /dev/input/ | grep js"')
			// grep ... output is // Uint8Array
			if (stringJsConnected.length==0) {
				// no devices found
				this._refreshList();
				return
			}
			var arrayJsConnected = ByteArray.toString(stringJsConnected)
			// build the list
			let listJs = []
			let nameJs=""
			for (let i = 0; i < arrayJsConnected.length; i++){
				if (arrayJsConnected[i]=="\n"){
					listJs.push(nameJs)
					nameJs=""
				} else {
					nameJs+=arrayJsConnected[i]
				}
			}
			// for every device try see if is a DS4
			for(let i=0; i < listJs.length; i++){
				//let string1 ='udevadm info -a -p $(udevadm info -q path -n /dev/input/js0) | egrep -o KERNELS==[[:punct:]][[:xdigit:]]{4}:054C:09CC\.[[:xdigit:]]{4}'
				//let string2 ='udevadm info -a -p $(udevadm info -q path -n /dev/input/js0) | egrep -o KERNELS==[[:punct:]][[:xdigit:]]{4}:054C:05C4\.[[:xdigit:]]{4}'
				let string1 ='/bin/bash -c "udevadm info -a -p $(udevadm info -q path -n /dev/input/'+listJs[i]+') | egrep -o KERNELS==[[:punct:]][[:xdigit:]]{4}:054C:09CC\.[[:xdigit:]]{4}"'
				let string2 ='/bin/bash -c "udevadm info -a -p $(udevadm info -q path -n /dev/input/'+listJs[i]+') | egrep -o KERNELS==[[:punct:]][[:xdigit:]]{4}:054C:05C4\.[[:xdigit:]]{4}"'
				// find kernel attrib (contain device DIR name)
				let [, stringKern09CC] = GLib.spawn_command_line_sync(string1)
				let [, stringKern05C4] = GLib.spawn_command_line_sync(string2)
				let string3 = '/bin/bash -c "udevadm info -a -p $(udevadm info -q path -n /dev/input/'+listJs[i]+') |  grep uniq"'
				let [, stringUname05C4] = GLib.spawn_command_line_sync(string3)
				var stringUname = ByteArray.toString(stringUname05C4)
				var stringDir
				if(stringKern05C4.length!=0){
					// associate unique name id at directory name
					stringDir = ByteArray.toString(stringKern05C4)
					stringDir=stringDir.substring(10,[29])
					stringUname=stringUname.substring(18,[35])
				} else {
					if(stringKern09CC.length!=0){
						// associate unique name id at directory name
						stringDir = ByteArray.toString(stringKern09CC)
						stringDir=stringDir.substring(10,[29])
						stringUname=stringUname.substring(18,[35])
					} else {
						//device not found
						stringDir=null
						stringUname="generic controller"
					}
				}
				this._listDevices.push(_Device(stringDir,stringUname,null,null,null,null))
			}
			this._refreshList();
		}

		_refreshList(){
			this._popupMenuExpander.menu.removeAll()
			// i=0 not needed, void device
			for (let i = 1; i < this._listDevices.length; i++){
				var text=this._listDevices[i].deviceUname
				for (let j = 0; j < this._database.length; j++){
					if(this._database[j].deviceUname==text){
						if (this._database[j].deviceRenamed!='new name'){
							text=this._database[j].deviceRenamed
						}
					}
				}
				let menuItem = new PopupMenu.PopupImageMenuItem(text,null);
				menuItem.connect('button-press-event',this._onDS4Selected.bind(this, i));
				this._popupMenuExpander.menu.addMenuItem(menuItem);
			}
			this._activeItem=0;
		}

		_onDS4Selected(ai){
			this._activeItem=ai;
			if (this._activeItem == -1)return
			//change current device to active item
			this._currentDevice.deviceDir = this._listDevices[this._activeItem].deviceDir
			this._currentDevice.deviceUname = this._listDevices[this._activeItem].deviceUname

			// verify exist a save state for the device selected
			for (let i = 0; i < this._database.length; i++){
				if(this._database[i].deviceUname==this._listDevices[this._activeItem].deviceUname) {
					// record device state
					this._currentDevice.red = this._database[i].red
					this._currentDevice.green = this._database[i].green
					this._currentDevice.blue = this._database[i].blue
		   			this._sendToDevice()
					this._UIelementsToCurrentColor()
					//change name
					var text=this._database[i].deviceUname
					if (this._database[i].deviceRenamed!='new name'){
						text=this._database[i].deviceRenamed
					}
					this._entry.clutter_text.set_text(text);
					this._popupMenuExpander.setSubmenuShown(true)
					return
				}
			}
			// a new item not found in database
			this._currentDevice.red = this._database[0].red
			this._currentDevice.green = this._database[0].green
			this._currentDevice.blue = this._database[0].blue
			//add device to database WITH slice in order to not copy a pointer
			this._database.push(_Device(null,this._currentDevice.deviceUname,'new name',this._currentDevice.red,this._currentDevice.green,this._currentDevice.blue))
			var i = this._database.length
			// add to database
			this._sendToDevice()
			this._UIelementsToCurrentColor()
			//change name
			var text=this._currentDevice.deviceUname
			this._entry.clutter_text.set_text(text);
		}

		_UIelementsToCurrentColor(){
			var color = new Gdk.RGBA();
			color.red = this._currentDevice.red
			color.green = this._currentDevice.green
			color.blue = this._currentDevice.blue
			color.alpha = 1
			//set colorbutton XXX pending
			// set sliders
			let v=Math.max(color.red,color.green,color.blue)
			let n=v-Math.min(color.red,color.green,color.blue);
			let h= n && ((v==color.red) ? (color.green-color.blue)/n : ((v==color.green) ? 2+(color.blue-color.red)/n : 4+(color.red-color.green)/n));
			let aaa = [60*(h<0?h+6:h), v&&n/v, v];
			h = aaa[0]/360
			let s = aaa[1]
			v = aaa[2]
			this._cSlider.value=h
			this._sSlider.value=s
			this._bSlider.value=v
		}

		_sendToDevice() {
			let g = Math.round(this._currentDevice.green*255);
			let r = Math.round(this._currentDevice.red*255);
			let b = Math.round(this._currentDevice.blue*255);
			if (this._currentDevice.deviceDir==null) {
				// Device not connected
				return 0;
			}
			//position program
			let directoryPath="/sys/class/leds/"+this._currentDevice.deviceDir+":green/";
			let directory = Gio.File.new_for_path(directoryPath);
			if (!directory.query_exists(null)) {
				//print('Error: directory '+this._currentDevice.deviceDir+':green not found');
			}else{
				let filePath = GLib.build_filenamev([directoryPath, "brightness"]);
				let value = ''+g
				let args = ["sh", "-c","echo "+ "\"" +value +"\"" + " > "+"\"" +filePath+"\"" +" "];
				GLib.spawn_sync(null, args, null, GLib.SpawnFlags.SEARCH_PATH, null);
			}
			directoryPath="/sys/class/leds/"+this._currentDevice.deviceDir+":red/";
			directory = Gio.File.new_for_path(directoryPath);
			if (!directory.query_exists(null)) {
				//print('Error: directory '+this._currentDevice.deviceDir+':red not found');
			}else{
				let filePath = GLib.build_filenamev([directoryPath, "brightness"]);
				let value = ''+r
				let args = ["sh", "-c","echo "+ "\"" +value +"\"" + " > "+"\"" +filePath+"\"" +" "];
				GLib.spawn_sync(null, args, null, GLib.SpawnFlags.SEARCH_PATH, null);
			}
			directoryPath="/sys/class/leds/"+this._currentDevice.deviceDir+":blue/";
			directory = Gio.File.new_for_path(directoryPath);
			if (!directory.query_exists(null)) {
				//print('Error: directory '+this._currentDevice.deviceDir+':blue not found');
			}else{
				let filePath = GLib.build_filenamev([directoryPath, "brightness"]);
				let value = ''+b
				let args = ["sh", "-c","echo "+ "\"" +value +"\"" + " > "+"\"" +filePath+"\"" +" "];
				GLib.spawn_sync(null, args, null, GLib.SpawnFlags.SEARCH_PATH, null);
			}
		}

		_saveStatus() {
			//save database
			for (let i = 0; i < this._database.length; i++){
				if(this._database[i].deviceUname==this._currentDevice.deviceUname) {
					// record device state
					this._database[i].red	= this._currentDevice.red
					this._database[i].green	= this._currentDevice.green
					this._database[i].blue	= this._currentDevice.blue
				}
			}
			let dataJSON = JSON.stringify(this._database);
			// Using .config in home
			let dataDir = GLib.get_user_config_dir();
			// current folder
			//dataDir = GLib.get_current_dir();
			let destination = GLib.build_filenamev([dataDir, 'DS4-Led', 'saved_conf.json'])
			let destinationFile = Gio.File.new_for_path(destination);
			var parent = destinationFile.get_parent();
			if (!parent.query_exists(null)){
				// folder doesn't exist
				// Creating conf saving directory
				parent.make_directory(null);
			}
			let [success, tag] = destinationFile.replace_contents(dataJSON, null, false, Gio.FileCreateFlags.REPLACE_DESTINATION, null);
			if(!success) {
				/* it failed */
				Main.notify('Example Notification', 'DS4 extension Save failed!')
				//print("File "+destination+"not saved")
			}
		}

		_loadStatus(){
			this._database = []
			//read file and push elements into the database of devices
			// using .config in home
			let dataDir = GLib.get_user_config_dir();
			// current folder
			// dataDir = GLib.get_current_dir();
			let destination = GLib.build_filenamev([dataDir, 'DS4-Led', 'saved_conf.json'])
			let destinationFile = Gio.File.new_for_path(destination);
			if (!destinationFile.query_exists(null)) {
				//Creating new database
				this._database.push(_Device(null,'Select DS4','new name',0,0,0.7))
			}else{
				var data = ByteArray.toString(GLib.file_get_contents(destination)[1])
				this._database = JSON.parse(data)
			}
		}
	}

	// Compatibility with gnome-shell >= 3.32
	if (SHELL_MINOR > 30) {
		ExampleIndicator = GObject.registerClass(
			{GTypeName: 'ExampleIndicator'},
			ExampleIndicator
		);
	}


	function init() {
		log(`initializing ${Me.metadata.name} version ${Me.metadata.version}`);
	}

	function enable() {
		log(`enabling ${Me.metadata.name} version ${Me.metadata.version}`);

		this._indicator = new ExampleIndicator();
		Main.panel.addToStatusArea(`${Me.metadata.name} Indicator`, this._indicator,0,'right');
	}

	function disable() {
		log(`disabling ${Me.metadata.name} version ${Me.metadata.version}`);

		// REMINDER: It's required for extensions to clean up after themselves when
		// they are disabled. This is required for approval during review!
		if (this._indicator !== null) {
			this._indicator.destroy();
			this._indicator = null;
		}
	}
